public class Train {
    int nTotSeats;
    int nFirstClassSeats;
    int nSecondClassSeats;
    int nFirstClassReservedSeats;
    int nSecondClassReservedSeats;

    void build(int firstClassSeats, int secondClassSeats) {
        this.nFirstClassSeats = firstClassSeats;
        this.nSecondClassSeats = secondClassSeats;
        this.nTotSeats = this.nFirstClassSeats + this.nSecondClassSeats;
        this.nFirstClassReservedSeats = 0;
        this.nSecondClassReservedSeats = 0;
        System.out.println(this.nTotSeats);
    }

    void reserveFirstClassSeats(int n) {
        this.nFirstClassReservedSeats += n;
    }

    void reserveSecondClassSeats(int n) {
        this.nSecondClassReservedSeats += n;
    }

    double getTotOccupancyRatio() {
        return ((double) this.nFirstClassReservedSeats + this.nSecondClassReservedSeats) / this.nTotSeats;
    }

    double getFirstClassOccupancyRatio() {
        return (double) this.nFirstClassReservedSeats / this.nFirstClassSeats;
    }

    double getSecondClassOccupancyRatio() {
        return (double) this.nSecondClassReservedSeats / this.nSecondClassSeats;
    }

    void deleteAllReservations() {
        this.nFirstClassReservedSeats = 0;
        this.nSecondClassReservedSeats = 0;
    }
}
