package it.unibo.oop.lab05.ex4;

import java.util.Comparator;
import it.unibo.oop.lab05.ex3.Product;

public class ProductComparator implements Comparator<Product> {
	
	public int compare(Product o1, Product o2) {
		return o1.compareTo(o2);
	}

}
