class Student {

    // ... Aggiungere qui la definizione dei campi
    String name;
    String surname;
    int id;
    int matriculationYear;

    void build(String name, String surname, int id, int year) {
        this.name = name;
        this.surname = surname;
        this.id = id;
        this.matriculationYear = year;
    }

    void printStudentInfo() {
        System.out.println("== INFO STUDENTE ==");
        System.out.println("Nome:\t" + this.name);
        System.out.println("Cognome:\t" + this.surname);
        System.out.println("Matricola:\t" + this.id);
        System.out.println("Anno immatricolazione:\t" + this.matriculationYear);
    }
}
