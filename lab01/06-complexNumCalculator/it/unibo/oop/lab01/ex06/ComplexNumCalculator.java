package it.unibo.oop.lab01.ex06;

public class ComplexNumCalculator {
    int nOpDone;
    ComplexNum lastRes;

    ComplexNum add(ComplexNum p1, ComplexNum p2) {
        this.lastRes.build(p1.re, p1.im);
        this.lastRes.add(p2);
        this.nOpDone++;
        return this.lastRes;
    }

    ComplexNum sub(ComplexNum p1, ComplexNum p2) {
        this.lastRes.build(p1.re, p1.im);
        this.lastRes.sub(p2);
        this.nOpDone++;
        return this.lastRes;
    }

    ComplexNum mul(ComplexNum p1, ComplexNum p2) {
        this.lastRes.build(p1.re, p1.im);
        this.lastRes.mul(p2);
        this.nOpDone++;
        return this.lastRes;
    }

    ComplexNum mod(ComplexNum p1) {
        this.lastRes.build(p1.mod(), 0);
        this.nOpDone++;
        return this.lastRes;
    }

    void build() {
        this.lastRes = new ComplexNum();
        this.lastRes.build(0, 0);
        this.nOpDone = 0;
    }
}
