package it.unibo.oop.lab05.ex2;

import java.util.Comparator;

class StringComparator implements Comparator<String> {

	public int compare(String o1, String o2) {
		boolean isInteger = true;
		for(int i = 0; i<o1.length() && isInteger ; i++) {
			isInteger = isInteger && (Character.isDigit(o1.charAt(i)) || o1.charAt(i) == '.');
		}
		for(int i = 0; i<o2.length() && isInteger ; i++) {
			isInteger = isInteger && (Character.isDigit(o2.charAt(i)) || o2.charAt(i) == '.');
		}
		if(isInteger) {
			double i1 = Double.parseDouble(o1);
			double i2 = Double.parseDouble(o2);
			if(i1<i2) {
				return -1;
			} else if(i1>i2) {
				return 1;
			} else {
				return 0;
			}

		} else {
			for(int i = 0; i < Math.min(o1.length(), o2.length()); i++) {
				if(o1.charAt(i) < o2.charAt(i)) {
					return -1;
				} else if(o1.charAt(i) > o2.charAt(i)) {
					return 1;
				}
			}
			return 0;
		}
	}

}
