package it.unibo.oop.lab05.ex4;

import java.util.TreeSet;
import java.util.Set;

import it.unibo.oop.lab05.ex3.WarehouseImpl;
import it.unibo.oop.lab05.ex3.Product;

public class OrderedWarehouse extends WarehouseImpl {
	public OrderedWarehouse() {
		super();
		this.storage = new TreeSet<Product>(new ProductComparator());
	}
}
