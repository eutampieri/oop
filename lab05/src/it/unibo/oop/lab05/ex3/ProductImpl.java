package it.unibo.oop.lab05.ex3;

public class ProductImpl implements Product {
	private String description;
	private String EAN13;
	private int quantity;
	
	public ProductImpl(String EAN13, String description) {
		this.EAN13 = EAN13;
		this.description = description;
	}
	
	/**
	 * Sell / add to stock q items
	**/
	public void modifyQuantity(int q) {
		this.quantity += q;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return this.EAN13 + "("+this.description+")";
	}

	public double getQuantity() {
		// TODO Auto-generated method stub
		return this.quantity;
	}
	
	public String toString() {
		return this.getQuantity() + "x \t"+this.getName();
	}
	
	public int compareTo(Product o) {
		return this.getName().compareTo(o.getName());
	}

}
