package it.unibo.oop.lab05.ex3;

/**
 * This interface represents a product.
 * 
 */
public interface Product extends Comparable<Product> {

    /**
     * @return the product name
     */
    String getName();

    /**
     * @return the amount of product
     */
    double getQuantity();

}
