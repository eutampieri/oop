package it.unibo.oop.lab01.ex06;
class ComplexNum {

    double re;
    double im;

    void build(double re, double im) {
        this.re = re;
        this.im = im;
    }

    void buildFrom(ComplexNum c) {
        this.re = c.re;
        this.im = c.im;
    }

    boolean equal(ComplexNum num) {
        return this.re == num.re && this.im == num.im;
    }

    void add(ComplexNum num) {
        this.re += num.re;
        this.im += num.im;
    }

    void sub(ComplexNum num) {
        this.re -= num.re;
        this.im -= num.im;
    }

    void mul(ComplexNum num) {
        this.re = (this.re * num.re - this.im * num.im);
        this.im = (this.re * num.im - this.im * num.re);
    }

    double mod() {
        return Math.sqrt(this.re * this.re + this.im * this.im);
    }

    String toStringRep() {
        return this.re + (this.im >= 0 ? " + " : " ") + this.im + "i";
    }
}
