**`TestScopes.build(int a, int b)`**
```
[build method:] I can access the field a (1), the field b (2), the input parameter a (1) and the input parameter b (2).
```
**`TestScopes.dummyMethod(int c)`**
```
[dummyMethod:] I can access the field a (1), the field b (2) and the input parameter c (3)."
```
**`TestScopes.dummyMethod2(int a)`**
```
[dummyMethod2:] I can access the field a (1), the field b (2), the input parameter a (4), and the local variable b (0).I have no access to c, dummyMethod's input parameter.
```