package it.unibo.oop.lab05.ex3;

import java.util.Set;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

public class WarehouseImpl implements Warehouse {
	protected Set<Product> storage;

	public WarehouseImpl() {
		this.storage = new LinkedHashSet<>();
	}
	public void addProduct(Product p) {
		this.storage.add(p);

	}

	public Set<String> allNames() {
		return this.storage
				.stream()
				.map(Product::getName)
				.collect(Collectors.toSet());
	}

	public Set<Product> allProducts() {
		return this.storage;
	}

	public boolean containsProduct(Product p) {
		return this.storage.contains(p);
	}

	public double getQuantity(String name) {
		for(var i: this.storage) {
			if(i.getName() == name) {
				return i.getQuantity();
			}
		}
		return -1;
	}

}
