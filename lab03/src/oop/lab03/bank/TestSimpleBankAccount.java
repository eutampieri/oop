package oop.lab03.bank;

public final class TestSimpleBankAccount {

    private TestSimpleBankAccount() { }

    public static void main(final String[] args) {
        /*
         * 1) Creare l' AccountHolder relativo a Mario Rossi con id 1 2) Creare
         * l' AccountHolder relativo a Luigi Bianchi con id 2 3) Creare i due
         * SimpleBankAccount corrispondenti 4) Effettuare una serie di depositi e
         * prelievi 5) Stampare a video l'ammontare dei due conti e verificare
         * la correttezza del risultato 6) Provare a prelevare fornendo un idUsr
         * sbagliato 7) Controllare nuovamente l'ammontare
         */
    	var mr = new AccountHolder("Mario", "Rossi", 1);
    	var lb = new AccountHolder("Luigi", "Bianchi", 2);
    	var mrAccount = new SimpleBankAccount(mr.getUserID(), 10.5);
    	var lbAccount = new SimpleBankAccount(lb.getUserID(), 0);
    	System.out.println(mrAccount);
    	System.out.println(lbAccount);
    	lbAccount.depositFromATM(lb.getUserID(), 100);
    	lbAccount.withdrawFromATM(lb.getUserID(), 20);
    	mrAccount.withdraw(mr.getUserID(), 20);
    	mrAccount.deposit(lb.getUserID(), 20);
    	System.out.println(mrAccount);
    	System.out.println(lbAccount);
    }
}
