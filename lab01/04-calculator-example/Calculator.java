public class Calculator {
    int nOpDone;
    double lastRes;

    double add(double p1, double p2) {
        this.lastRes = p1 + p2;
        this.nOpDone++;
        return this.lastRes;
    }

    double sub(double p1, double p2) {
        this.lastRes = p1 - p2;
        this.nOpDone++;
        return this.lastRes;
    }

    double mul(double p1, double p2) {
        this.lastRes = p1 * p2;
        this.nOpDone++;
        return this.lastRes;
    }

    double div(double p1, double p2) {
        this.lastRes = p1 / p2;
        this.nOpDone++;
        return this.lastRes;
    }

    /// Perché serve?
    void build() {
        this.lastRes = 0.0;
        this.nOpDone = 0;
    }
}
